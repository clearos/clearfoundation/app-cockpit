<?php

/**
 * MariaDB settings controller.
 *
 * @category   apps
 * @package    cockpit
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/cockpit/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

// Exceptions
//-----------

use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * MariaDB settings controller.
 *
 * @category   apps
 * @package    cockpit
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/cockpiit/
 */

class Setting extends ClearOS_Controller
{
    /**
     * MariaDB default controller
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->load->library('cockpit/Cockpit');
        $this->lang->load('cockpit');

        // Set validation rules
        //---------------------

        if ($this->input->post('submit')) {
            $this->form_validation->set_policy('current_password', 'cockpit/Cockpit', 'validate_password', TRUE);
            $this->form_validation->set_policy('password', 'cockpit/Cockpit', 'validate_password', TRUE);
            $this->form_validation->set_policy('verify', 'cockpit/Cockpit', 'validate_password', TRUE);
        } else {
            $this->form_validation->set_policy('new_password', 'cockpit/Cockpit', 'validate_password', TRUE);
            $this->form_validation->set_policy('new_verify', 'cockpit/Cockpit', 'validate_password', TRUE);
        }

        $form_ok = $this->form_validation->run();

        // Extra validation
        //-----------------

        if ($this->input->post('submit')) {
            $current_password = $this->input->post('current_password');
            $password = $this->input->post('password');
            $verify = $this->input->post('verify');
        } else {
            $current_password = '';
            $password = $this->input->post('new_password');
            $verify = $this->input->post('new_verify');
        }

        if ($form_ok) {
            if ($password !== $verify) {
                $this->form_validation->set_error('new_verify', lang('base_password_and_verify_do_not_match'));
                $this->form_validation->set_error('verify', lang('base_password_and_verify_do_not_match'));
                $form_ok = FALSE;
            }
        }

        // Load view data
        //---------------

        try {
            $is_running = $this->cockpit->get_running_state();
            $data['url_cockpit'] = $this->cockpit->get_url_cockpit();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('cockpit/setting', $data, lang('base_settings'));
    }
}
