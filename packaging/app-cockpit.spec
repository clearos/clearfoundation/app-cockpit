
Name: app-cockpit
Epoch: 1
Version: 1.0.0
Release: 1%{dist}
Summary: Cockpit Server
License: GPLv3
Group: Applications/Apps
Packager: ClearFoundation
Vendor: ClearFoundation
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Cockpit is a management server focused on speed, utility, security, and flexibility.

%package core
Summary: Cockpit Server - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-base-core >= 1:1.2.6
Requires: app-network-core
Requires: app-storage-core >= 1:1.4.7
Requires: cockpit >= 173.2-1

%description core
Cockpit is a management server focused on speed, utility, security, and flexibility.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/cockpit
cp -r * %{buildroot}/usr/clearos/apps/cockpit/

install -d -m 0755 %{buildroot}/var/clearos/cockpit
install -d -m 0755 %{buildroot}/var/clearos/cockpit/backup
install -D -m 0644 packaging/cockpit.php %{buildroot}/var/clearos/base/daemon/cockpit.php
install -D -m 0644 packaging/cockpit_default.conf %{buildroot}/etc/clearos/storage.d/cockpit_default.conf

%post
logger -p local6.notice -t installer 'app-cockpit - installing'

%post core
logger -p local6.notice -t installer 'app-cockpit-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/cockpit/deploy/install ] && /usr/clearos/apps/cockpit/deploy/install
fi

[ -x /usr/clearos/apps/cockpit/deploy/upgrade ] && /usr/clearos/apps/cockpit/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-cockpit - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-cockpit-core - uninstalling'
    [ -x /usr/clearos/apps/cockpit/deploy/uninstall ] && /usr/clearos/apps/cockpit/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/cockpit/controllers
/usr/clearos/apps/cockpit/htdocs
/usr/clearos/apps/cockpit/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/cockpit/packaging
%exclude /usr/clearos/apps/cockpit/unify.json
%dir /usr/clearos/apps/cockpit
%dir /var/clearos/cockpit
%dir /var/clearos/cockpit/backup
/usr/clearos/apps/cockpit/deploy
/usr/clearos/apps/cockpit/language
/usr/clearos/apps/cockpit/libraries
/var/clearos/base/daemon/cockpit.php
/etc/clearos/storage.d/cockpit_default.conf
